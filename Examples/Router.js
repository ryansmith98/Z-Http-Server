"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const zServer = require("z-http-server");
const zRouter = require("z-http-router");
const l = require("simple-logging");
//Initialise server
const server = new zServer.Server({
    port: 3004
});
const router = new zRouter.Router();
server
    .use((ctx) => __awaiter(this, void 0, void 0, function* () {
    l.info(ctx);
    return ctx;
}))
    .routeHandler = router.handler;
router
    .get('/', (ctx) => __awaiter(this, void 0, void 0, function* () {
    ctx.send('This is a get request');
    return ctx;
}))
    .use('post', '/', (ctx) => __awaiter(this, void 0, void 0, function* () {
    ctx.send('This is a post request');
    return ctx;
}))
    .use('all', '/', (ctx) => __awaiter(this, void 0, void 0, function* () {
    ctx.send('This could be any request - will only be used for anything that is' +
        ' not get or post because we have already defined them');
    return ctx;
}));
//Start the server
server.start();
//# sourceMappingURL=Router.js.map