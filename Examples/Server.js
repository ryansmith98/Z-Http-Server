"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const zServer = require("z-http-server");
const l = require("simple-logging");
//Initialise server
const server = new zServer.Server({
    port: 3004
});
server
    .use((ctx) => __awaiter(this, void 0, void 0, function* () {
    l.info(ctx);
    return ctx;
}))
    .routeHandler = (ctx) => __awaiter(this, void 0, void 0, function* () {
    ctx.status(200);
    ctx.send('<html><body><h1>Hi</h1></body></html>');
    return ctx;
});
//Start the server
server.start();
//# sourceMappingURL=Server.js.map