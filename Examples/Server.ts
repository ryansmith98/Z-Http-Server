﻿import * as zServer from 'z-http-server'

//Initialise server
const server = new zServer.Server( {
  port: 3004
} )

server
  //Add some middleware to log ctx
  .use( async ctx => {
    console.log( ctx )
    return ctx
  } )
  //Set the route handler to retturn html
  .routeHandler = async ctx => {
    ctx.status( 200 )
    ctx.send( '<html><body><h1>Hi</h1></body></html>')
    return ctx
  }
//Start the server
server.start()
