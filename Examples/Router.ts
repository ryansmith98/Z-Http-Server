﻿import * as zServer from 'z-http-server'
import * as zRouter from 'z-http-router'

//Initialise server
const server = new zServer.Server( {
  port: 3004
} )

const router = new zRouter.Router()

server
  //Add some middleware to log ctx
  .use( async ctx => {
    console.log( ctx )
    return ctx
  } )
  //Set the route handler to retturn html
  .routeHandler = router.handler

router
  .get( '/', async ctx => {
    ctx.send('This is a get request')
    return ctx
  } )
  .use( 'post', '/', async ctx => {
    ctx.send('This is a post request')
    return ctx
  } )
  .use( 'all', '/', async ctx => {
    ctx.send( 'This could be any request - will only be used for anything that is' +
      ' not get or post because we have already defined them' )
    return ctx
  } )

//Start the server
server.start()
