[![NPM](https://nodei.co/npm/z-http-router.png)](https://nodei.co/npm/z-http-router/)

# z-http-router
This is a router to use with z-http-server. There is also an improved error handler as I didn't see a reason to create a new module for it.

To get started:

```
npm install --save z-http-router
```

or

```
yarn add z-http-router
```

Then:

```
const zServer = require('z-http-server')
const zRouter = require('z-http-router')

const server = new zServer.Server( { port: 3004 } )
const router = new zRouter.Router()
server.routeHandler = router.handler

router.get( '/', async ctx => {
  ctx.send('Hello')
  return ctx
} )

router.get( '/hi', async ctx => { ctx.send('hi'); return ctx })

server.start()
```

# Adding routes
There are three types of routes you can add:

* get
* post
* all

These are all added using the respective function.

Example:

```
router.get(path, middlewareFunction)
```

Or

```
router.use(method, path, middlewareFunction)
```

# Error handler
To set an error message:

```
router.errors['statusCode'] = 'This is an error message'
```

The error object that is thrown must have the message set as the status code e.g. ```throw new Error( '404' )```.

# static
A static directory can be set to serve static files such as images, stylesheets, etc.

To set a static directory do the following:

```
router.static('path/to/static/folder', '/path/to/access')
```

To reference a file in that folder, use:

```
/path/to/access/filename.file
```