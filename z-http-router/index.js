"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = require("fs");
const path_1 = require("path");
const mime = require("mime-types");
class Router {
    constructor() {
        this.handler = (ctx) => {
            return new Promise((resolve, reject) => {
                let { method } = ctx;
                if (this.routes[method.toLowerCase()] === undefined) {
                    method = 'all';
                }
                else if (this.routes[method.toLowerCase()][ctx.path] === undefined) {
                    method = 'all';
                }
                if (this.routes[method.toLowerCase()][ctx.path] === undefined) {
                    let isStatic = false;
                    Object.keys(this.staticRoutes)
                        .forEach(r => {
                        if (ctx.path.indexOf(r) === 0) {
                            isStatic = true;
                            const p = ctx.path.replace(r, '');
                            const fp = this.staticRoutes[r];
                            fs_1.readFile(path_1.join(fp, p), 'utf-8', (err, content) => {
                                if (err) {
                                    reject(err);
                                    return;
                                }
                                ctx.res.writeHead(200, { 'Content-Type': mime.contentType(path_1.extname(path_1.join(fp, p))) || 'application/octet-stream' });
                                ctx.res.end(content);
                                resolve(ctx);
                            });
                        }
                    });
                    if (!isStatic) {
                        reject(new Error('404'));
                    }
                    return;
                }
                this.routes[method.toLowerCase()][ctx.path](ctx)
                    .then(resolve)
                    .catch(reject);
            });
        };
        this.errorHandler = (err, ctx) => __awaiter(this, void 0, void 0, function* () {
            ctx.status(parseInt(err.message));
            if (this.errors.hasOwnProperty(err.message)) {
                ctx.send(this.errors[err.message]);
            }
            else {
                ctx.send('An error has occurred');
            }
        });
        this.routes = {
            get: {},
            post: {},
            all: {}
        };
        this.staticRoutes = {};
        this.errors = {
            '404': 'Page not found',
            '500': 'An error has occurred'
        };
    }
    get(path, handler) {
        this.routes.get[path] = handler;
        return this;
    }
    post(path, handler) {
        this.routes.post[path] = handler;
        return this;
    }
    all(path, handler) {
        this.routes.all[path] = handler;
        return this;
    }
    use(method, path, handler) {
        this.routes[method.toLowerCase()][path] = handler;
        return this;
    }
    static(folderRoot, urlPath) {
        this.staticRoutes[urlPath] = path_1.resolve(folderRoot);
        return this;
    }
}
exports.Router = Router;
//# sourceMappingURL=index.js.map