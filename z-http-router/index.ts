﻿import * as zServer from 'z-http-server'
import { readFile } from 'fs'
import { join, resolve, extname } from 'path'
import * as mime from 'mime-types'

export interface Routes {
  get: Object
  post: Object
  all: Object
}

export type Method = 'get' | 'GET' | 'post' | 'POST' | 'all' | 'ALL'

export class Router {
  public routes: Routes
  public staticRoutes: Object
  public errors: Object
  constructor() {
    this.routes = {
      get: {},
      post: {},
      all: {}
    }
    this.staticRoutes = {}
    this.errors = {
      '404': 'Page not found',
      '500': 'An error has occurred'
    }
  }
  handler = ( ctx: zServer.ServerContext ): Promise<zServer.ServerContext> => {
    return new Promise( ( resolve, reject ) => {
      let { method } = ctx
      if ( this.routes[method.toLowerCase()] === undefined ) {
        method = 'all'
      }
      else if ( this.routes[method.toLowerCase()][ctx.path] === undefined ) {
        method = 'all'
      }
      if ( this.routes[method.toLowerCase()][ctx.path] === undefined ) {
        let isStatic = false
        Object.keys( this.staticRoutes )
          .forEach( r => {
            if ( ctx.path.indexOf( r ) === 0 ) {
              isStatic = true
              const p = ctx.path.replace( r, '' )
              const fp = this.staticRoutes[r]
              readFile( join( fp, p ), 'utf-8', ( err, content ) => {
                if ( err ) { reject( err ); return }
                ctx.res.writeHead( 200, { 'Content-Type': mime.contentType( extname( join( fp, p ) ) ) || 'application/octet-stream' } )
                ctx.res.end( content )
                resolve( ctx )
              } )
            }
          } )
        if ( !isStatic ) {
          reject( new Error( '404' ) )
        }
        return
      }
      this.routes[method.toLowerCase()][ctx.path]( ctx )
        .then( resolve )
        .catch( reject )
    } )
  }
  errorHandler = async ( err: Error, ctx: zServer.ServerContext ) => {
    ctx.status( parseInt( err.message ) )
    if ( this.errors.hasOwnProperty( err.message ) ) { ctx.send( this.errors[err.message] ) }
    else { ctx.send( 'An error has occurred' ) }
  }
  get( path: string, handler: zServer.ServerMiddleware ) {
    this.routes.get[path] = handler
    return this
  }
  post( path: string, handler: zServer.ServerMiddleware) {
    this.routes.post[path] = handler
    return this
  }
  all( path: string, handler: zServer.ServerMiddleware ) {
    this.routes.all[path] = handler
    return this
  }
  use( method: Method, path: string, handler: zServer.ServerMiddleware ) {
    this.routes[method.toLowerCase()][path] = handler
    return this
  }
  static( folderRoot: string, urlPath: string ) {
    this.staticRoutes[urlPath] = resolve( folderRoot )
    return this
  }
}
