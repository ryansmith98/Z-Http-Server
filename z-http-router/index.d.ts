import * as zServer from 'z-http-server';
export interface Routes {
    get: Object;
    post: Object;
    all: Object;
}
export declare type Method = 'get' | 'GET' | 'post' | 'POST' | 'all' | 'ALL';
export declare class Router {
    routes: Routes;
    staticRoutes: Object;
    errors: Object;
    constructor();
    handler: (ctx: zServer.ServerContext) => Promise<zServer.ServerContext>;
    errorHandler: (err: Error, ctx: zServer.ServerContext) => Promise<void>;
    get(path: string, handler: zServer.ServerMiddleware): this;
    post(path: string, handler: zServer.ServerMiddleware): this;
    all(path: string, handler: zServer.ServerMiddleware): this;
    use(method: Method, path: string, handler: zServer.ServerMiddleware): this;
    static(folderRoot: string, urlPath: string): this;
}
