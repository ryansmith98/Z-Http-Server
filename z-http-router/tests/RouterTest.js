﻿const chai = require( 'chai' )
const chaihttp = require( 'chai-http' )
const zServer = require( 'z-http-server' )
const Server = zServer.Server
const zRouter = require( '../index' )
const Router = zRouter.Router

const { expect } = chai
chai.use( chaihttp )

describe( "Start server on port 3004", () => {
  let server
  let router
  beforeEach( () => {
    server = new Server( {
      port: 3004
    } )
    server.createServer()
    router = new Router()
  } )


  it( 'with one get route', done => {
    let method = ''
    server.routeHandler = router.handler
    router.get( '/', async ctx => {
      method = ctx.method
      ctx.send('Hi')
      return ctx
    } )

    chai
      .request( server.server )
      .get( '/' )
      .end( ( err, res ) => {
        expect( method ).not.to.equal( '' )
        expect( res.text ).to.equal( 'Hi' )
        done()
      } )
  } )

  it( 'with one post route', done => {
    let method = ''
    server.routeHandler = router.handler
    router.post( '/', async ctx => {
      method = ctx.method
      ctx.send( 'Hi' )
      return ctx
    } )

    chai
      .request( server.server )
      .post( '/' )
      .end( ( err, res ) => {
        expect( method ).not.to.equal( '' )
        expect( res.text ).to.equal( 'Hi' )
        done()
      } )
  } )

  it( 'with one all route', done => {
    let method = ''
    server.routeHandler = router.handler
    router.all( '/', async ctx => {
      method = ctx.method
      ctx.send( 'Hi' )
      return ctx
    } )

    chai
      .request( server.server )
      .post( '/' )
      .end( ( err, res ) => {
        expect( method ).not.to.equal( '' )
        expect( res.text ).to.equal( 'Hi' )
        chai
          .request( server.server )
          .get( '/' )
          .end( ( err, res ) => {
            expect( method ).not.to.equal( '' )
            expect( res.text ).to.equal( 'Hi' )
            done()
          } )
      } )
  } )

  it( 'with one get route with use', done => {
    let method = ''
    server.routeHandler = router.handler
    router.use( 'get', '/', async ctx => {
      method = ctx.method
      ctx.send( 'Hi' )
      return ctx
    } )

    chai
      .request( server.server )
      .get( '/' )
      .end( ( err, res ) => {
        expect( method ).not.to.equal( '' )
        expect( res.text ).to.equal( 'Hi' )
        done()
      } )
  } )

  it( 'with non existent route', done => {
    server.routeHandler = router.handler

    chai
      .request( server.server )
      .get( '/' )
      .end( ( err, res ) => {
        expect( res.serverError ).to.equal( true )
        done()
      } )
  } )

  it( 'with one put route', done => {
    let method = ''
    server.routeHandler = router.handler
    router.all( '/', async ctx => {
      method = ctx.method
      ctx.send( 'Hi' )
      return ctx
    } )

    chai
      .request( server.server )
      .put( '/' )
      .end( ( err, res ) => {
        expect( method ).not.to.equal( '' )
        expect( res.text ).to.equal( 'Hi' )
        done()
      } )
  } )
} )
