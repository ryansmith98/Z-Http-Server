#! /bin/bash
command=$1
projects=( "z-http-server" "z-http-router" )

case $command in 
  "deps")
    for proj in ${projects[@]}
    do
      cd $proj
      yarn install
      cd ../
    done
    ;;

  "compile")
    for proj in ${projects[@]}
    do
      cd $proj
      tsc
      cd ../
    done
    ;;
  
  "test")
    for proj in ${projects[@]}
    do
      cd $proj
      yarn run test
      cd ../
    done
    ;;
  
  "coverage")
    for proj in ${projects[@]}
    do
      cd $proj
      yarn run coverage
      cd ../
    done
    ;;
esac