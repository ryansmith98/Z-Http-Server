/// <reference types="node" />
import * as http from 'http';
export declare class ServerContext {
    req: http.ServerRequest;
    res: http.ServerResponse;
    send: (contents: string) => void;
    setHeader: (name: string, value: string | string[]) => void;
    status: (statusCode: number) => void;
    writeHead: any;
    write: any;
    end: any;
    method: string;
    path: string;
    url: string;
    params: any;
    redirect: (path: string) => void;
}
export interface ServerOptions {
    port?: number;
    logging?: 'all' | 'error' | 'warn' | 'none';
    parallelMiddleware?: Boolean;
}
export declare type ServerMiddleware = (ctx: ServerContext) => Promise<ServerContext>;
export declare type errorHandler = (err: Error, ctx: ServerContext) => Promise<void>;
export declare class Server {
    server: http.Server;
    middleware: ServerMiddleware[];
    config: ServerOptions;
    routeHandler: ServerMiddleware;
    errorHandler: errorHandler;
    constructor(options?: ServerOptions);
    use(middleware: ServerMiddleware): this;
    start(port?: number): Server;
    createServer(): this;
    stop(): Server;
    private log(level, ...contents);
}
