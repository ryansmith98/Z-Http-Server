﻿import * as http from 'http'
import { parse } from 'querystring'

export class ServerContext {
  req: http.ServerRequest
  res: http.ServerResponse
  send: ( contents: string ) => void
  setHeader: ( name: string, value: string | string[] ) => void
  status: ( statusCode: number ) => void
  writeHead
  write
  end
  method: string
  path: string
  url: string
  params
  redirect: ( path: string ) => void
}

export interface ServerOptions {
  port?: number
  logging?: 'all' | 'error' | 'warn' | 'none'
  parallelMiddleware?: Boolean
}

const defaultOptions: ServerOptions = {
  port: 80,
  logging: 'all',
  parallelMiddleware: false
}

function defaultRouteHandler( ctx: ServerContext ): Promise<ServerContext> {
  return new Promise( res => {
    ctx.send('The server works!!!')
    res( ctx )
  })
}
function defaultErrorHandler( err: Error, ctx: ServerContext ): Promise<void> {
  return new Promise( res => {
    ctx.status( 500 )
    ctx.send( JSON.stringify(err) )
    res()
  })
}

export type ServerMiddleware = ( ctx: ServerContext ) => Promise<ServerContext>
export type errorHandler = ( err: Error, ctx: ServerContext ) => Promise<void>

export class Server {
  public server: http.Server
  public middleware: ServerMiddleware[]
  public config: ServerOptions
  public routeHandler: ServerMiddleware
  public errorHandler: errorHandler
  constructor( options: ServerOptions = { } ) {
    this.config = { }
    Object.assign( this.config, defaultOptions, options )
    this.routeHandler = defaultRouteHandler
    this.errorHandler = defaultErrorHandler
    this.middleware = []
  }
  use( middleware: ServerMiddleware ) {
    this.middleware.push( middleware )
    return this
  }
  start( port: number = this.config.port ): Server {
    this.createServer()
    this.server.on( 'error', e => { throw e } )
    this.server.listen( port )
    this.log( 'log', 'Started server on', port )
    return this
  }
  createServer() {
    this.server = http.createServer( async ( request, response ) => {
      const ctx: ServerContext = {
        req: request,
        res: response,
        send: ( contents: string ) => {
          response.write( contents );
          response.end()
        },
        writeHead: ( statusCode: number, reasonPhrase?: string, headers?: http.OutgoingHttpHeaders ) => { response.writeHead( statusCode, reasonPhrase, headers ) },
        write: ( str: string, cb?: Function ) => { return response.write( str, cb ) },
        end: ( str?: string, cb?: Function ) => { if ( str === undefined ) { response.end() } else { response.end( str, cb ) } },
        setHeader: ( name: string, value: string | string[] ) => { response.setHeader( name, value ) },
        status: ( statusCode?: number ) => response.statusCode = statusCode,
        method: request.method,
        url: request.url,
        path: request.url.split( '?' )[0],
        params: parse( '?' + request.url.split( '?' )[1] ),
        redirect: ( path: string ) => { response.writeHead( 302, { 'Location': path } ); response.end() }
      }

      let p: Promise<ServerContext>
      let c: ServerContext = ctx
      let error = false

      if ( this.config.parallelMiddleware ) {
        const ps = []
        this.middleware.forEach( mw => {
          ps.push( mw( ctx ) )
        } )
        await Promise.all( ps )
          .then( ctxs => {
            ctxs.forEach( nextCtx => {
              Object.assign( ctx, nextCtx )
            } )
          } )
          .catch( e => { this.errorHandler( e, ctx ); error = true } )
      }
      else {
        for ( let i = 0; i < this.middleware.length; i++ ) {
          p = this.middleware[i]( c )
          await p.then( retc => c = retc ).catch( e => { this.errorHandler( e, ctx ); error = true } )
        }
      }

      if ( !error ) {
        this.routeHandler( c )
          .catch( e => {
            this.errorHandler( e, ctx )
          } )
      }
    } )
    return this
  }
  stop(): Server {
    if ( this.server === undefined ) {
      this.log( 'warn', 'Server has not been started, so it cannot be stopped' )
      return this
    }
    this.server.close()
    this.server = undefined
    return this
  }
  private log( level: 'log' | 'error' | 'warn', ...contents ) {
    if ( this.config.logging !== 'all' ) {
      if ( this.config.logging !== level ) { return }
    }
    console[level].apply( null, contents )
  }
}