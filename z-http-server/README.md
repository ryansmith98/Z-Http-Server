[![NPM](https://nodei.co/npm/z-http-server.png)](https://nodei.co/npm/z-http-server/)

# Z-Http-Server
The idea of this http server is to be unique and simpler than existing servers. It should also be easy to extend and build on.

This project is inspired by express and koa.

# Getting started
To start using the server, first install it:

```npm install --save z-http-server```

or

```yarn add z-http-server```

Once it's installed it can be used as below:

```
const zServer = require('z-http-server')
const server = new zServer.Server( { port: 3004 } )
server.routeHandler = async ctx => {
  ctx.send('Hi')
  return ctx
}
server.start()
```

# The Server class
The whole server is exported as ```z-http-server.Server```. This class has the following properties:

* server - A normal http server object
* config - The options object
* middleware - An array of middleware functions
* config - The config object
* routeHandler - The function that handles each request
* errorHandler - The function that handles any error that comes from middleware of requestHandler

It also has the following functions - all of them except constructor return ```this```:

* constructor(options) - The constructor
* use(middlewareFunction) - Adds a middleware function to the middleware array
* start(port - defaults to port defined in config) - Runs createServer and then starts it
* stop() - Stops the server
* createServer() - Creates a server instance on server.server without starting it
* log(level, message) - The internal logging function

# Options
## port
The port that the server listens on. This defaults to 80.

This port can be overrided in the start function.

## logging
The logging level. This defaults to ```all```.
Levels are:

* ```all``` - log, warn, and error
* ```warn``` - only warn
* ```error``` - only error

## parallelMiddleware
This is a boolean value that defaults to false. This may be unstable as I haven't tested it yet. There should be a speed increase with middleware if it works though.

# Context
On each new request the routeHandler is called with a context object. The context object has the following properties:

* req - The http request object
* res - The http response object
* method - The http method
* path - The requested path
* url - The full requested url
* params - The parameters

It also has the following functions:

* send(contents - string) - Writes contents to the response then ends it
* setHeader - The http setHeader function
* status(statusCode - number) - Sets the status code
* writeHead - The http writeHead function
* write - The http write function
* end - The http end function

# Middleware
Every middleware function must take in a context object and return a promise that resolves the context object.

This is so that if the context object is modified in any way, that change is not lost.

Example:
```
function mw(ctx) {
  return new Promise(function(resolve, reject) {
    resolve(ctx)
  })
}
```

#routeHandler
The route handler has the same signature as a middleware function. The default route handler sands the string ```The server works!!!```.

This is obviously intended to be changed when it is used.

It can be changed as follows:

```
server.routeHandler = function(ctx) {/*blah*/}
```

#errorHandler
The error handler has a similar signature to a middleware function, it takes in an error object and a context object. It returns a promise that resolves nothing.

Example:

```
server.errorHandler = function(err, ctx) {/*blah*/}
```

The default error handler send the error as a string with a status code 500. This may work for development but should be changed for any proper use of the server.