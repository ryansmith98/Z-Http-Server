﻿const chai = require( 'chai' )
const chaihttp = require( 'chai-http' )
const zServer = require( '../index' )
const Server = zServer.Server

const { expect } = chai
chai.use( chaihttp )

describe( "Start server on port 3004", () => {
  let server
  beforeEach( () => {
    server = new Server( {
      port: 3004
    } )
    server.createServer()
  } )

  it( 'with one middleware', done => {
    let method = ''
    server.use( async ctx => { method = ctx.method; return ctx } )
    chai
      .request( server.server )
      .get( '/' )
      .end( ( err, res ) => {
        expect( method ).not.to.equal( '' )
        done()
      } )
  } )

  it( 'with two middlewares', done => {
    let method = ''
    let path = ''
    server.use( async ctx => { method = ctx.method; return ctx } )
    server.use( async ctx => { path = ctx.path; return ctx } )
    chai
      .request( server.server )
      .get( '/' )
      .end( ( err, res ) => {
        expect( method ).not.to.equal( '' )
        expect( path ).not.to.equal( '' )
        done()
      } )
  } )

  it( 'with two middlewares in parallel', done => {
    let method = ''
    let path = ''
    server = new Server( {
      port: 3004,
      parallelMiddleware: true
    } )
    server.createServer()

    server.use( async ctx => { method = ctx.method; return ctx } )
    server.use( async ctx => { path = ctx.path; return ctx } )

    chai
      .request( server.server )
      .get( '/' )
      .end( ( err, res ) => {
        expect( method ).not.to.equal( '' )
        expect( path ).not.to.equal( '' )
        done()
      } )
  } )

  it( 'with a route handler', done => {
    let method = ''
    server.routeHandler = async ctx => { method = ctx.method; ctx.send( 'Hi' ); return ctx }
    chai
      .request( server.server )
      .get( '/' )
      .end( ( err, res ) => {
        expect( method ).not.to.equal( '' )
        done()
      } )
  } )

  it( 'with middleware causing an error', done => {
    let e
    server.use( async ctx => { e = new Error( 'Hi' ); throw e } )
    chai
      .request( server.server )
      .get( '/' )
      .end( (err, res) => {
        expect( res.serverError ).to.equal( true )
        done()
      })
  } )

  it( 'with a redirect', done => {
    let method = ''
    server.routeHandler = async ctx => {
      if ( ctx.path === '/' ) { ctx.redirect( '/re' ) }
      if ( ctx.path === '/re' ) { method = ctx.method; ctx.send( 're' ) }
    }
    chai
      .request( server.server )
      .get( '/' )
      .end( ( err, res ) => {
        expect( method ).not.to.equal( '' )
        expect( res.text ).to.equal( 're' )
        done()
      } )
  } )
} )
