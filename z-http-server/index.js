"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const http = require("http");
const querystring_1 = require("querystring");
class ServerContext {
}
exports.ServerContext = ServerContext;
const defaultOptions = {
    port: 80,
    logging: 'all',
    parallelMiddleware: false
};
function defaultRouteHandler(ctx) {
    return new Promise(res => {
        ctx.send('The server works!!!');
        res(ctx);
    });
}
function defaultErrorHandler(err, ctx) {
    return new Promise(res => {
        ctx.status(500);
        ctx.send(JSON.stringify(err));
        res();
    });
}
class Server {
    constructor(options = {}) {
        this.config = {};
        Object.assign(this.config, defaultOptions, options);
        this.routeHandler = defaultRouteHandler;
        this.errorHandler = defaultErrorHandler;
        this.middleware = [];
    }
    use(middleware) {
        this.middleware.push(middleware);
        return this;
    }
    start(port = this.config.port) {
        this.createServer();
        this.server.on('error', e => { throw e; });
        this.server.listen(port);
        this.log('log', 'Started server on', port);
        return this;
    }
    createServer() {
        this.server = http.createServer((request, response) => __awaiter(this, void 0, void 0, function* () {
            const ctx = {
                req: request,
                res: response,
                send: (contents) => {
                    response.write(contents);
                    response.end();
                },
                writeHead: (statusCode, reasonPhrase, headers) => { response.writeHead(statusCode, reasonPhrase, headers); },
                write: (str, cb) => { return response.write(str, cb); },
                end: (str, cb) => { if (str === undefined) {
                    response.end();
                }
                else {
                    response.end(str, cb);
                } },
                setHeader: (name, value) => { response.setHeader(name, value); },
                status: (statusCode) => response.statusCode = statusCode,
                method: request.method,
                url: request.url,
                path: request.url.split('?')[0],
                params: querystring_1.parse('?' + request.url.split('?')[1]),
                redirect: (path) => { response.writeHead(302, { 'Location': path }); response.end(); }
            };
            let p;
            let c = ctx;
            let error = false;
            if (this.config.parallelMiddleware) {
                const ps = [];
                this.middleware.forEach(mw => {
                    ps.push(mw(ctx));
                });
                yield Promise.all(ps)
                    .then(ctxs => {
                    ctxs.forEach(nextCtx => {
                        Object.assign(ctx, nextCtx);
                    });
                })
                    .catch(e => { this.errorHandler(e, ctx); error = true; });
            }
            else {
                for (let i = 0; i < this.middleware.length; i++) {
                    p = this.middleware[i](c);
                    yield p.then(retc => c = retc).catch(e => { this.errorHandler(e, ctx); error = true; });
                }
            }
            if (!error) {
                this.routeHandler(c)
                    .catch(e => {
                    this.errorHandler(e, ctx);
                });
            }
        }));
        return this;
    }
    stop() {
        if (this.server === undefined) {
            this.log('warn', 'Server has not been started, so it cannot be stopped');
            return this;
        }
        this.server.close();
        this.server = undefined;
        return this;
    }
    log(level, ...contents) {
        if (this.config.logging !== 'all') {
            if (this.config.logging !== level) {
                return;
            }
        }
        console[level].apply(null, contents);
    }
}
exports.Server = Server;
//# sourceMappingURL=index.js.map